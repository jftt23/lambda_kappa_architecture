FROM bde2020/hadoop-namenode:2.0.0-hadoop3.2.1-java8

# Copy the job executor script
COPY /job_execute.sh .

# Copy the batch mode script
COPY /run_batch_mode.sh .

# Copy the asap executor script
COPY /asap_execute.sh .

CMD ["/run_batch_mode.sh"]
