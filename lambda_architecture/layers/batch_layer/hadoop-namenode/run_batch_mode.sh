#!/bin/bash

# Different modes
interval="interval"
asap="asap"
manual="manual"

# deactivate safe mode
hdfs dfsadmin -safemode leave

# delete deprecated logs
# Block length cannot be obtained
hdfs dfs -rm -r /logs

if [[ $REPLAY_MODE == "$interval" ]]
then
	echo "Interval Mode selected"
    
    	# Install Cron
    	apt-get update && apt-get -y install cron

    	# Execute at minute 0
    	echo "0 * * * * root /job_execute.sh" >> /etc/cron.d/root
    
    	# Change File Mode
    	chmod 600 /etc/cron.d/root

    	# Start cronjob
    	/usr/bin/crontab /etc/cron.d/root    
    	/usr/sbin/cron start
fi

if [[ $REPLAY_MODE == "$asap" ]]
then
	echo "ASAP Mode selected"
    
	# Run script in background
	/asap_execute.sh &
else
    echo "Manual Mode selected"
fi

# Run script
/run.sh

