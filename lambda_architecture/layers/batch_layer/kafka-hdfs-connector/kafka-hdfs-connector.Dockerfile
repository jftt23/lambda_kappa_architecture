FROM confluentinc/cp-kafka-connect-base

# Install HDFS3-Kafka-Connector
RUN confluent-hub install --no-prompt confluentinc/kafka-connect-hdfs3:1.1.9

# Copy Configuration File
COPY hdfs3-parquet-field.json .

# Run script to establish connector
COPY start_connector.sh .

EXPOSE 8083
EXPOSE 8888
