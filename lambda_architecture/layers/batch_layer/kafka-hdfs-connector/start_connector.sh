# Launch Kafka Connect
/etc/confluent/docker/run &

# Wait for Kafka Connect listener
echo "Waiting for Kafka Connect to start listening on connect ⏳"
while : ; do
  curl_status=$(curl -s -o /dev/null -w %{http_code} http://kafka-connect:8083/connectors)
  echo -e $(date) " Kafka Connect listener HTTP state: " $curl_status " (waiting for 200)"
  if [ $curl_status -eq 200 ] ; then
    break
  fi
  sleep 5 
done

echo -e "\n--\n+> Creating Data Generator source"

# Post request with configuration file to initialize connector
curl --request POST \
     --url http://kafka-connect:8083/connectors \
     --header 'Accept: application/json' \
     --header 'Content-Type: application/json' \
     --data @hdfs3-parquet-field.json

sleep infinity

