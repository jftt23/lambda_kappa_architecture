#!/bin/bash

hadoop fs -mkdir       /tmp
hadoop fs -mkdir -p    /user/hive/warehouse
hadoop fs -chmod g+w   /tmp
hadoop fs -chmod g+w   /user/hive/warehouse

nohup $HIVE_HOME/bin/hive --service hiveserver2 --hiveconf hive.server2.enable.doAs=false & 

echo "Creating Table!"

./create_table.sh
