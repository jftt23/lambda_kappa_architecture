FROM bde2020/hive:2.3.2-postgresql-metastore

COPY create_table.sh create_table.sh
COPY createTable.sql createTable.sql

RUN nohup bash -c "./create_table.sh &" 

