package mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.util.StringTokenizer;

import models.Tweet;
import deserialization.TweetSchema;

public class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

    private final IntWritable one = new IntWritable(1);
    private final Text word = new Text();
    private final TweetSchema schema = new TweetSchema();

    public void map(Object key, Text value, Mapper.Context context) {
        StringTokenizer itr = new StringTokenizer(value.toString(), "\n");
        while (itr.hasMoreTokens()) {
            try {
                Tweet tweet = schema.deserialize(itr.nextToken());
                System.out.println(tweet);
                for (String hashtag : tweet.hashtags) {
                    word.set(hashtag);
                    context.write(word, one);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}