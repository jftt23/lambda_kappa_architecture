import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import reducer.IntSumReducer;
import mapper.TokenizerMapper;

public class BatchJob {
    public static void main(String[] args) throws Exception {
        Job job = Job.getInstance();
        job.setJarByClass(BatchJob.class);
        job.setJobName("Hashtag Count");
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.setInputDirRecursive(job, true);
        FileInputFormat.addInputPath(job, new Path("/topics/tweets"));
        FileSystem fs = FileSystem.get(new Configuration());
        fs.delete(new Path("/job_result"), true);
        FileOutputFormat.setOutputPath(job, new Path("/job_result"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}