package deserialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Tweet;

public class TweetSchema {
    final ObjectMapper objectMapper;

    public TweetSchema() {
        this.objectMapper = new ObjectMapper();
    }

    public Tweet deserialize(String input) throws Exception {
        return objectMapper.readValue(input, Tweet.class);
    }
}
