package deserialization;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import models.Tweet;

public class TweetSchema implements KafkaDeserializationSchema<Tweet> {
    final ObjectMapper objectMapper;

    public TweetSchema() {
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public boolean isEndOfStream(Tweet tweet) {
        return false;
    }

    @Override
    public Tweet deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
        return objectMapper.readValue(consumerRecord.value(), Tweet.class);
    }

    @Override
    public TypeInformation<Tweet> getProducedType() {
        return TypeInformation.of(Tweet.class);
    }
}
