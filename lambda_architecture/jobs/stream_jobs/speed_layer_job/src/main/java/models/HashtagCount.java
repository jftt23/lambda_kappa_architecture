package models;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;

@Table(keyspace = "results", name = "hashtagcount2")
public class HashtagCount {

    @Column(name = "hashtag")
    private String hashtag = "";

    @Column(name = "count")
    private long count = 0;

    public HashtagCount() {}

    public HashtagCount(String hashtag, long count) {
        this.setHashtag(hashtag);
        this.setCount(count);
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return getHashtag() + " : " + getCount();
    }
}