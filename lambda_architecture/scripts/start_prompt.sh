#!/bin/bash

OPTION1=batch
OPTION2=realtime

PRESTO_CONNECTION=localhost:8080
SERVING_LAYER_NETWORK=build_default
CASSANDRA_CONTAINER_NAME=cassandra

if [ $# -ne 1 ]; 
then
	echo "Please specify where you want to query from: realtime (real-time views), batch (serving layer)"  
	exit -1
fi

if [ ! -f "./.presto.jar" ]
then
	wget https://repo1.maven.org/maven2/com/facebook/presto/presto-cli/0.275/presto-cli-0.275-executable.jar
	mv presto-cli-0.275-executable.jar .presto.jar
	chmod u+x .presto.jar 
fi

if [ "$1" = "$OPTION1" ];
then
	sudo ./.presto.jar --server "$PRESTO_CONNECTION" --catalog hive --schema default
fi

if [ "$1" = "$OPTION2" ];
then
	sudo docker run -it --network $SERVING_LAYER_NETWORK --rm cassandra cqlsh $CASSANDRA_CONTAINER_NAME  
fi

