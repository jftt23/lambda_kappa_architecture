#!/bin/bash

cd ..

if [ -d .build ]
then
	rm -rf .build
fi

mkdir .build

cp -r layers/batch_layer/* .build/
cp -r layers/speed_layer/* .build/
cp -r layers/serving_layer/* .build/
cp -r jobs .build/hadoop-namenode/jobs/
cp docker-compose.yaml .build/

cd .build

sudo docker-compose \
	-f docker-compose.yaml \
	-f batch_layer.docker-compose.yaml \
	-f serving_layer.docker-compose.yaml \
	-f speed_layer.docker-compose.yaml \
	up \
	--remove-orphans \
	--build 

