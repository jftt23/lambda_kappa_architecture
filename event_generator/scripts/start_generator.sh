#!/bin/bash

# Load Util File
. .util.sh

thread_number=1
ticks_per_second=1
network="build_default"

env="-e THREAD_NUMBER=$thread_number -e TICKS_PER_SECOND=$ticks_per_second"

sudo docker run \
	--network $network \
	--name kafka_event_generator \
	$env \
	"kafka_event_generator:$GENERATOR_VERSION"
