#!/bin/bash

# Load Util File
. .util.sh

# Stop Generator if its still running
if [[ ! -z $(sudo docker ps -aqf "name=$CONTAINER_NAME") ]]; then
	./stop_generator.sh
fi

echo "removing image"

# Remove docker image
sudo docker image rm $IMAGE_NAME

echo "image removed"
