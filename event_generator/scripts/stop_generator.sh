#!/bin/bash
 
CONTAINER_NAME=kafka_event_generator
 
# get container Id based on the container name
CONTAINER_ID=$(sudo docker ps -aqf "name=$CONTAINER_NAME")

echo "Stopping kafka_event_generator" 

# stop container
sudo docker stop $CONTAINER_ID
sudo docker rm $CONTAINER_ID

echo "kafka_event_generator stopped"
