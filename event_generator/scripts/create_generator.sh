#!/bin/bash

# Load Util File
. .util.sh

# change directory to root of generator
cd ..

# inrement generator version 
GENERATOR_VERSION=$((GENERATOR_VERSION + 1))

# update generator version
echo $GENERATOR_VERSION > .generator.version

echo "Create Generator with version nr: $GENERATOR_VERSION"

# create build directory
mkdir build

# Copy content into build directory
cp -r kafka_event_generator/* build/
cp -r data/* build/ 
cp Dockerfile build/

# build image
sudo docker build --no-cache -t "kafka_event_generator:$GENERATOR_VERSION" ./build

# delete build directory
rm -rf build
