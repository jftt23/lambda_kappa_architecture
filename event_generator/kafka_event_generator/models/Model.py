from abc import abstractstaticmethod


class Model:
    @abstractstaticmethod
    def from_string(self, object):
        pass
