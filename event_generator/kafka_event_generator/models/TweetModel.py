from models.Model import Model
from datetime import datetime
import json
from bson import json_util

date_format = "%a %b %d %H:%M:%S %z %Y"

class TweetEvent(Model):
    index_mapping = {
        "tweetId": 0,
        "username": 1,
        "timestamp": 2,
        "followers": 3,
        "friends": 4,
        "retweets": 5,
        "favorites": 6,
        "entities": 7,
        "sentiment": 8,
        "mentions": 9,
        "hashtags": 10,
        "urls": 11
    }
    
    def __init__(self, object, _=index_mapping):
        self.tweet_id = object[_["tweetId"]]
        self.username = object[_["username"]]
        self.timestamp = datetime.strptime(object[_["timestamp"]],date_format)
        self.followers = int(object[_["followers"]])
        self.friends = int(object[_["friends"]])
        self.retweets = int(object[_["retweets"]])
        self.favorites = int(object[_["favorites"]])
        self.entities = object[_["entities"]].split(";")
        self.sentiment = [ int(sentiment) for sentiment in object[_["sentiment"]].split(" ")]
        self.mentions = object[_["mentions"]].split(" ")
        self.hashtags = object[_["hashtags"]].split(" ")
        self.urls = object[_["urls"]].split(":-: ")

    def toString(self):
        return str(json.dumps(self.__dict__, default=json_util.default))