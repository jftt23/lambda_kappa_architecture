from threading import Thread, Timer
from kafka import KafkaProducer
from datetime import timedelta


class RequestWorker(Thread):
    def __init__(self, count, queue, offset, position, brokers, ticks_per_second, start_date):
        Thread.__init__(self)
        self.producer = KafkaProducer(bootstrap_servers=brokers)
        self.position = position
        self.thread_nr = count
        self.thread_position = offset + position
        self.brokers = brokers
        self.offset = offset
        self.queue = queue
        self.ticks = ticks_per_second
        assert(self.ticks > 0)
        self.timespan = 10
        self.start_date = start_date
        self.current_date = start_date
        self.current_event = queue[self.thread_position]

    def run(self):
        self.request()

    def request(self):
        if(self.thread_position < len(self.queue)):
            while(self.current_event.timestamp < self.current_date):
                self.producer.send("tweets", value=str.encode(self.current_event.toString()))
                print(f'''id: {self.current_event.toString()} ]''')
                self.thread_position += self.thread_nr
                if(self.thread_position < len(self.queue)):
                    exit
                self.current_event = self.queue[self.thread_position]
            self.current_date = self.current_date + timedelta(seconds=1)
            Timer(1 / self.ticks, self.request).start()
