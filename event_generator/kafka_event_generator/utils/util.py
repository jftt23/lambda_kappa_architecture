from math import floor
from datetime import datetime, timedelta


date_format = "%Y-%m-%d %H:%M:%S"

def find_offset_by_date(entries, predicate):
    min = 0
    max = len(entries)
    pointer = int(floor(max / 2))
    
    result = predicate(entries[pointer].meldedatum)

    while result != 0 and (max - min) != 1:
        if result == 0:
            return (0, pointer)
        elif result > 0:
            max = pointer
            pointer = min + int(((pointer - min) / 2))
        else:
            min = pointer
            pointer = pointer + int(((max-pointer) / 2))

        result = predicate(entries[pointer].meldedatum)
    return (-1, pointer)


def comparator_times(time):
    def anon_func(ct):
        diff = (datetime.strptime(time, date_format) - ct)
        zero = timedelta(days=0, hours=0, minutes=0, seconds=0)
        if diff == zero:
            return 0
        elif diff > zero:
            return -1
        else:
            return 1
    return anon_func