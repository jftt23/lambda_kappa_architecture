import csv
import sys
import getopt
import os
from datetime import datetime
from models.TweetModel import TweetEvent
from utils.util import comparator_times, find_offset_by_date, date_format
from workers.RequestWorker import RequestWorker


if __name__ == "__main__":

    with open('covid_tweets.csv', newline='') as csvfile:
        csv.field_size_limit(sys.maxsize)
        entries = list(map(lambda entry: TweetEvent(entry), list(csv.reader(csvfile, delimiter='\t'))[1:]))
        
        offset = 0
        start_date = entries[offset].timestamp
        for i in range(6):
            worker = RequestWorker(count=6, queue=entries, offset=offset,position=i,brokers=['kafka-2:10092','kafka-1:10091'], ticks_per_second=30, start_date=start_date)
            worker.start()
