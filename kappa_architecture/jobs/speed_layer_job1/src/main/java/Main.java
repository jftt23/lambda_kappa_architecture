import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.cassandra.CassandraSink;
import com.datastax.driver.mapping.Mapper;

import models.Tweet;
import models.HashtagCount;
import deserialization.TweetSchema;
import mapper.SplitterFlatMap;

public class Main {
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        final String brokers = "kafka-1:10091,kafka-2:10092";
        final String topic = "tweets";

        DataStream<HashtagCount> stream = env
                .fromSource(KafkaSource.<Tweet>builder()
                    .setBootstrapServers(brokers)
                    .setTopics(topic)
                    .setDeserializer(KafkaRecordDeserializationSchema.of(new TweetSchema()))
                    .setStartingOffsets(OffsetsInitializer.earliest())
                    .build(),
                        WatermarkStrategy.noWatermarks(),
                        "Kafka Source")
                .flatMap(new SplitterFlatMap())
                .keyBy(HashtagCount::getHashtag)
                .reduce((a,b) -> new HashtagCount(a.getHashtag(), a.getCount() + a.getCount()));

        CassandraSink.addSink(stream)
                .setHost("speed_layer_views")
                .setMapperOptions(() -> new Mapper.Option[]{Mapper.Option.saveNullFields(true)})
                .build();

        env.execute("First Speed Layer Job");
    }
}
