package mapper;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import java.util.Locale;

import models.Tweet;
import models.HashtagCount;

public class SplitterFlatMap implements FlatMapFunction<Tweet, HashtagCount> {
    @Override
    public void flatMap(Tweet tweet, Collector<HashtagCount> out) throws Exception {
        for (String hashtag : tweet.hashtags) {
            if (hashtag.equals("null;")) continue;
            out.collect(new HashtagCount(hashtag.toLowerCase(Locale.ROOT), 1));
        }
    }
}