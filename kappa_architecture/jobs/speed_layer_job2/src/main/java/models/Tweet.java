package models;

import java.util.List;

public class Tweet {
    public String tweet_id;
    public String username;
    public Object timestamp;
    public int followers;
    public int friends;
    public int retweets;
    public int favorites;
    public List<String> entities;
    public List<Integer> sentiment;
    public List<String> mentions;
    public List<String> hashtags;
    public List<String> urls;
}
